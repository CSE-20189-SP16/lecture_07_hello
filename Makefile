all:		hello.exe

hello_.c:	hello.c
	@echo "Preprocessing...."
	cpp hello.c > hello_.c

hello.s:	hello_.c
	@echo "Compiling..."
	gcc -o hello.s -S hello_.c

hello.o:	hello.s
	@echo "Assembling..."
	as -o hello.o hello.s

hello.exe:	hello.o
	@echo "Linking..."
	ld -dynamic-linker /lib64/ld-linux-x86-64.so.2 -o hello.exe /usr/lib64/crt1.o /usr/lib64/crti.o hello.o -lc /usr/lib64/crtn.o

clean:
	rm -f hello.exe hello.s hello.o hello_.c
